# 浙政钉对接模块使用手册

## 1 浙江乡村大脑单点登录
### 1.1 浙政钉单点登录

> 注意：本代码不提供code的获取;

code由前端调用如下地址获取：
```shell
https://szsn.zjagri.cn/gateway/oauth2/authorize?response_type=code&client_id=xxxxxxxx&scope=userinfo&redirect_uri=https://xxxxx
```

浏览器访问上述地址，返回如下：
```shell
https://xxx.com?code=Q1OSVtedLUv8nDPvt5bE8ige6ZxpRkKtQYIZBf9bHht3h2SnLTphP2Fqolxx
```

前端获取到code后，即可调用服务端的接口，获取浙政钉的用户信息及服务端的token；

使用方法：

```php
$code = $_GET['code'];
$appKey = '';
$appSecret = '';
$zzd = new XcdnLogin($appKey, $appSecret);
$user = $zzd->getUserInfo($code);
```

接下来可以将`$user`信息存入本地数据库，或者其他用途，返回本应用的token等信息给前端，达到第三方登录的功能。